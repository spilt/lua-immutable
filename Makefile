# makefile for immutable table library for Lua

LUA_DIR=/usr/local
LUA_INC= $(LUA_DIR)/include
LUA_BIN= $(LUA_DIR)/bin
LUA= lua

CC= gcc
CFLAGS= $(INCS) $(WARN) -O2 $G
WARN= -std=c11 -pedantic -Wall -Wextra
INCS= -I$(LUA_INC)
ifeq ($(shell uname -s),Darwin)
	MAKESO= $(CC) -bundle -undefined dynamic_lookup
else
	MAKESO= $(CC) -shared
endif

MYNAME= immutable
MYLIB= l$(MYNAME)
T= $(MYNAME).so
OBJS= $(MYLIB).o
TEST= tests.lua

all: $T

test: $T
	$(LUA_BIN)/$(LUA) $(TEST)

o:	$(MYLIB).o

so:	$T

$T:	$(OBJS)
	$(MAKESO) -o $@ $(OBJS)

$(OBJS): $(MYLIB).c

clean:
	rm -f $(OBJS) $T

# eof
